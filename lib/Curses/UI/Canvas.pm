
# ----------------------------------------------------------------------
# Curses::UI::Label
#
# (c) 2001-2002 by Maurice Makaay. All rights reserved.
# This file is part of Curses::UI. Curses::UI is free software.
# You can redistribute it and/or modify it under the same terms
# as perl itself.
#
# Currently maintained by Marcus Thiesen
# e-mail: marcus@cpan.thiesenweb.de
# ----------------------------------------------------------------------

# TODO: fix dox

package Curses::UI::Canvas;

use v5.24;
use warnings;
use experimental 'signatures';

use Curses;
use Curses::UI::Common;
use parent 'Curses::UI::Widget';

sub new ($package, @rest) {
   my %args = (
      -parent          => undef,    # the parent window
      -width           => undef,    # the width of the label
      -height          => undef,    # the height of the label
      -x               => 0,        # the hor. pos. rel. to the parent
      -y               => 0,        # the vert. pos. rel. to the parent
      -text            => undef,    # the text to show
      -textalignment   => undef,    # left / middle / right
      -paddingspaces   => 0,        # Pad text with spaces?
      -bg              => -1,
      -fg              => -1,
      @rest,
      -nocursor        => 1,        # This widget uses no cursor
      -focusable       => 0,        # This widget can't be focused
   );
   my $self = $package->SUPER::new(%args);
   $self->layout;
   return $self;
}

sub layout ($self) {
   $self->SUPER::layout or return;
   return $self;
}

sub bold            { shift()->set_attribute('-bold', shift())      }
sub reverse         { shift()->set_attribute('-reverse', shift())   }
sub underline       { shift()->set_attribute('-underline', shift()) }
sub dim             { shift()->set_attribute('-dim', shift())       }
sub blink           { shift()->set_attribute('-blink', shift())     }

sub set_attribute ($self, $key, $value) {
   $self->{$key} = $value;
   $self->intellidraw;
   return $self;
}

sub fg ($self, @new) {
   $self->{-fg} = $new[0] if @new > 0;
   return $self->{-fg};
}

sub text ($self, @new) {
   if (@new > 0) {
      $self->{-text} = $new[0];
      $self->intellidraw;
   }
   return $self->{-text};
}

sub draw ($self, $skip_doupdate = undef) {
   my $self = shift;
   my $skip_doupdate = shift;
   $self->SUPER::draw(1) or return $self;

   my $win = $self->{-canvasscr};

    # Clear all attributes.
    $win->attroff(A_REVERSE);
    $win->attroff(A_BOLD);
    $win->attroff(A_UNDERLINE);
    $win->attroff(A_BLINK);
    $win->attroff(A_DIM);

    # Set wanted attributes.
    $win->attron(A_REVERSE)   if $self->{-reverse};
    $win->attron(A_BOLD)      if $self->{-bold};
    $win->attron(A_UNDERLINE) if $self->{-underline};
    $win->attron(A_BLINK)     if $self->{-blink};
    $win->attron(A_DIM)       if $self->{-dim};

   if ($Curses::UI::color_support) {
      my $co = $Curses::UI::color_object;
      my $pair = $co->get_color_pair(
                           $self->{-fg},
                           $self->{-bg});
      $win->attron(COLOR_PAIR($pair));
   }

   my $width = $self->canvaswidth;
   my $height = $self->canvasheight;
   my $text = $self->{-text} // '';
   my @stuff = ref($text) ? $text->@* : split(m{}mxs, $text);
   for my $ri (0 .. $#stuff) {
      last if $ri >= $height;
      my $string = $stuff[$ri];# =~ s{[^\-\w:]}{.}rgmxs;
      $string = substr($string, 0, $width) if length($string) > $width;
      $win->addstring($ri, 0, $string);
   }
   $win->noutrefresh;

   doupdate() unless $skip_doupdate;
   return $self;
}

1;
