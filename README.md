A simple "Pomodoro" for the terminal.

# COPYRIGHT & LICENSE

With the exceptions in the following sub-sections, the contents of this
repository are licensed according to the Apache License 2.0 (see file `LICENSE`
in the project's root directory):

>  Copyright 2023,2024 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

## `smmono9.tlf`

The repository contains a copy of file [`smmono9.tlf`][smmono9], a font for
[figlet][]/[toilet][] that contains this:

```
================================================================================
  This font was automatically generated using:
   % caca2tlf --quarter --utf8 "Monospace 9"
================================================================================
```

Font [Monospace][] is licensed as [SIL Open Font License (OFL)][OFL].

[OFL]: https://openfontlicense.org/
[smmono9]: https://github.com/xero/figlet-fonts/blob/master/smmono9.tlf
[figlet]: http://www.figlet.org/
[toilet]: http://caca.zoy.org/wiki/toilet
[Monospace]: https://www.fontspace.com/monospace-font-f13274
