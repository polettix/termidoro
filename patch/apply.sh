#!/bin/sh

md="$(dirname "$(readlink -f "$0")")"
rd="$(dirname "$md")"

cd "$rd"
for f in "$md"/*.patch; do
   patch -p 0 <"$f"
done
